# Simple Distributed File System

This project aims to build a simple distributed file system with replication and fault tolernece. The project is build with Java Spring Boot and is composed of two nodes: 

- Name Server: Server that will contain the meta data about the files
- Storage Node: Node that will save the data to the disk


### Starting the REST Project

#### Start the Servers
The project is built and configured using docker compose. It includes the following structure:

```
    .
    ├── docker-compose.yml
    ├── name
    │   ├── Dockerfile
    │   ├── ...
    │   └── src
    └── storage
        ├── Dockerfile
        ├── ...
        └── src    
```

To build it, use the following commands:

```bash
cd files/rest/docker
docker-compose up 
```

Each server has its own "jar file" which is a compiled version of the coed as well as a Dockerfile which is similar between server. The compose has three storages and a name server, these are referenced by name. Using these names it is possible to start or stop them as explained in the previous section.

The available containers names are:

- **mongo** : The Mongo Database.
- **name** : The Name Server.
- **storage** : Storage Server 1.
- **storage2** : Storage Server 2.
- **storage3** : Storage Server 3.


### Architecture

The general idea is that we have a naming server that the client connects to in order to get the address of the storage server that contains the needed file or enough space for him to save his file. In both cases, the naming server returns a JSON that we will refer to as "Token" for the rest of this report. This token contains the information needed to perform the operation that the client needs and also serves as a mean for the name server to execute operations on the storage server. When a user requests a write operation and uploads a file it is replicated into another storage server to redundancy. 


### Overview

The REST version is built on top of the Spring Framework. It is a Web Framework that supports RESTful APIs and that provides an abstraction of the communication with the databases. Each Server is a Spring Boot Application which means that it listens in a specific port for HTTP Requests to specific endpoints. These endpoints are defined in the "Rest Controllers". The Servers communication using these APIs and the Client simply sends his requests to these endpoints depending on the operation. The messages are sent in JSON and the files are sent as octet streams, this communication is secured over HTTPS with each server having a Self Signed Certificate.

Each Spring Application contains a set of Java Classes, they are regrouped by functionalities. The projects are divided into folders, one for each function with the following layout:

```
.
    ├── config                    --> Configurations, Settings and Scheduled actions
    ├── error                     --> Exception Declarations
    ├── model                     --> Model Objects (Data base Entites, Transfer Objects)
    ├── repository                --> Data base Access Objects (To access the database)
    ├── rest                      --> Rest Endpoints Handlers 
    ├── security                  --> Security Configurations, Endpoints and Controllers
    ├── service                   --> Services Controllers (Logic of the Application) 
    ├── utils                     --> Commonly used functions and utilites
    └── <Server>Application.java  --> Starting Point of the Application
      
```   


The general flow of communication between the server can be seen as the following protocol:

1) The Client sends a query to the Naming Server.
2) The Naming server responds with an Operation Token.
3) The Client uses this token to know the address of the node.
4) The Client sends the token with any data if it is a write operation.
5) The server responds with an HTTP message.


To implement this, we divided each one of the servers into components, these components include REST interface for communication and controllers which are referred to as "Services", these handle the logic of the application. In the end, we were left with the division below. 

### Name Server

The Name Server is divided into multiple parts, each part handles one of the aspects of the DFS. Additionally, it is connected to a *Mongo* database for storing the files tree structure. The component of the name server is:

- Entity Service: This service handle the name resolution, it is the abstract layer that directly interacts with the client through the REST API.
- Node Service: This Service is responsible for electing the nodes for reading and writing, slave and master.
- Storage Service: This Service handles the persistence of the data in the database.
- Recovery Service: This service handles the Recovery Process explained in the following section0.
- Duplication Service: This service handle the Distribution Process explained below.

To allow the user and the storage nodes to use these services it exposes the following endpoints:

1) User Endpoints:

    - `[GET]` : *storage?path={path to file}* : Read the file with the path sent as a parameter.

    - `[POST]` : *storage* with JSON of meta data : Write the file with meta data into the storage.

    - `[DELETE]` : *storage?path={path to file}* : Delete the file with the path sent as a parameter.

    - `[GET]` : *properties?path={path to file}* : Get the properties of the file with the path sent as a parameter.

    - `[GET]` : *list?path={path to file}* : Get the list of files in the directory with the path sent as a parameter.

2) Nodes Endpoints:

    - `[POST]` : *nodes/register* with JSON of node Info : Register the node using its info.

    - `[POST]` : *nodes/commit* with Operation Token : Allow the node to notify the Name Server that it completed an operation.

    - `[GET]` : *nodes/status* : Get the status of a node (used for pinging the nodes).

When the client wants to access a resource, the name server checks the operation than get the appropriate node for it, prepares an Operation Token and sends it as a response. The client then uses this token to both know to which node to talk and as a "token" to be allowed to perform the action. It also serves to make the storage server have as little logic as possible which makes their replication easier.

When Storage Servers are started they try to register to the Name Server by sending their status which includes their capacity, their current free space and their ID using the endpoint provided above. The Name Server then decides what to do with the node. The process of selection the new state can be seen in *service/nodes/NodeManagementService.class* in the function **registerNode**. The Name Server keeps track of these storage nodes and iteratively pings them to check their status. This is scheduled to be every 1 second and the function for it can be found in *service/nodes/NodeManagementService.class* in the function **pingNode**.


### Storage Server

The Storage Server is also divided into services which handle different parts, these are:

- State Manager Service: saves the state into a file, updates it and reads it when needed.
- File Manager Service: Write and Reads files from the hard disk in a directory called "uploads".

The available operations for the storage server are:

- `[GET]` : *storage/ping* : Get the status of a node (for the name server to check if the storage server is alive).
- `[POST]` : *storage/download* with Operation Token : Get a file using an Operation Token.
- `[POST]` : *storage/upload* with Operation Token and File: Writes a file into the Storage Server.
- `[POST]` : *storage/execute* with Operation Token and File: Executes whatever operation is sent to by the Operation token.

All of these operations are executed with an operation token. This token is generated by the Name Server and given to the client to be used. In it, the client can find the address of the node and the server can find the operation information. In an actual implementation, the token would be signed and timestamped to prevent any abuse of the storage server without the consent of the name server. The information that it should include are:

- The path of the file which serves as an Identifier for the file.
- The operation itself (read, write, delete).
- A Timestamp that can be later used to avoid a replay of these operations.
- A Digest that can be used to validate that the token was issued by the name server.
- The size of the file to avoid the user exceeding the given size to the server.

Additionally, the delete operation was "secured" by requesting a token from the client. This token is only given to the client after sending its credentials to the name server.

