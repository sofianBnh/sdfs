package su.os3.std1.gdfs.storagenodeserver.service.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.Operation;

public interface IStorageStateManager {

    void executeOperation(Operation operation) throws ServiceException;

    Resource download(Operation operation) throws ServiceException;

    void upload(Operation operation, MultipartFile file) throws ServiceException;

}
