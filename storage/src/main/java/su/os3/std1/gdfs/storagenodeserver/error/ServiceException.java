package su.os3.std1.gdfs.storagenodeserver.error;

public class ServiceException extends Exception {
    public ServiceException(String message) {
        super(message);
    }
}
