package su.os3.std1.gdfs.storagenodeserver.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import su.os3.std1.gdfs.storagenodeserver.config.SupportedOperations;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.Operation;
import su.os3.std1.gdfs.storagenodeserver.service.storage.IStorageStateManager;

import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.*;

@Controller
public class FileAccessController {

    private final IStorageStateManager stateManagementService;

    @Autowired
    public FileAccessController(
            IStorageStateManager stateManagementService
    ) {

        this.stateManagementService = stateManagementService;
    }


    @PostMapping("storage/download")
    public ResponseEntity<?> download(@RequestBody Operation operation)
            throws ServiceException {

        operation.verify();

        if (operation.getOperation() != SupportedOperations.READ
                && operation.getOperation() != SupportedOperations.PULL)

            throw new ServiceException(WRONG_OPERATION);


        return ResponseEntity.ok()
                .contentType(FILES_TYPE)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;")
                .body(stateManagementService.download(operation));
    }

    @PostMapping(value = "storage/upload", consumes = {"multipart/form-data"})
    public ResponseEntity<?> upload(
            @RequestParam("file") MultipartFile file,
            @RequestParam("operation") String operationJson
    ) throws ServiceException {

        Operation operation = Operation.fromString(operationJson);

        if (operation == null)
            throw new ServiceException(MALFORMED_OPERATION);

        operation.verify();

        if (operation.getOperation() != SupportedOperations.WRITE)
            throw new ServiceException(WRONG_OPERATION);

        stateManagementService.upload(operation, file);
        return ResponseEntity.ok("");
    }

    @PostMapping("storage/execute")
    public ResponseEntity<?> execute(@RequestBody Operation operation)
            throws ServiceException {

        operation.verify();

        if (!EXECUTABLE_OPERATIONS.contains(operation.getOperation()))
            throw new ServiceException(WRONG_OPERATION);

        stateManagementService.executeOperation(operation);
        return ResponseEntity.ok("");
    }

    @GetMapping("storage/ping")
    public ResponseEntity<?> ping() {
        return ResponseEntity.ok("");
    }


}
