package su.os3.std1.gdfs.storagenodeserver.service.state;

import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.NodeStatus;

public interface IStateManager {

    void writeState(NodeStatus current) throws ServiceException;

    NodeStatus readState() throws ServiceException;

    boolean isStateAvailable() throws ServiceException;

    void writeInitialState() throws ServiceException;

    void formatNodeState() throws ServiceException;

}
