package su.os3.std1.gdfs.storagenodeserver.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.ErrorResponse;

@ControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<?> serviceExceptions(ServiceException exception) {
        return ResponseEntity.badRequest().body(new ErrorResponse(exception.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> generalExceptions(Exception exception) {
        return ResponseEntity.badRequest().body(new ErrorResponse(exception.getMessage()));
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<?> httpException(HttpClientErrorException exception) {

        String errorJson = exception.getResponseBodyAsString();
        ErrorResponse error;

        error = ErrorResponse.fromString(errorJson);

        if (error == null)
            error = new ErrorResponse(exception.getMessage());


        return ResponseEntity.badRequest().body(error);
    }


}
