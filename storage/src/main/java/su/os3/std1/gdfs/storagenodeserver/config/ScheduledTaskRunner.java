package su.os3.std1.gdfs.storagenodeserver.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.NodeStatus;
import su.os3.std1.gdfs.storagenodeserver.model.RegistrationAcknowledgment;
import su.os3.std1.gdfs.storagenodeserver.service.files.IFileManagerService;
import su.os3.std1.gdfs.storagenodeserver.service.rest_client.IRestClientService;
import su.os3.std1.gdfs.storagenodeserver.service.state.IStateManager;

import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.*;

@Component
public class ScheduledTaskRunner {

    private final IStateManager stateManager;
    private final IRestClientService restClientService;
    private final IFileManagerService fileManagerService;
    private final Logger logger = LoggerFactory.getLogger(ScheduledTaskRunner.class);
    private boolean registered;
    private int attemptCounter;

    @Autowired
    public ScheduledTaskRunner(
            IStateManager stateManager,
            IRestClientService restClientService,
            IFileManagerService fileManagerService
    ) {
        this.fileManagerService = fileManagerService;
        this.restClientService = restClientService;
        this.stateManager = stateManager;
        this.registered = false;
        this.attemptCounter = 0;
    }


    @Scheduled(fixedRate = REGISTRATION_RETRY_INTERVAL)
    public void resetAttemptCounter() {

        if (attemptCounter == RETRY_ATTEMPTS)
            this.attemptCounter = 0;
    }


    @Scheduled(fixedRate = REGISTRATION_INTERVAL)
    public void register() {

        if (!registered && this.attemptCounter < RETRY_ATTEMPTS) {

            try {

                if (!stateManager.isStateAvailable())
                    stateManager.writeInitialState();

                NodeStatus state = stateManager.readState();

                RegistrationAcknowledgment acknowledgment = restClientService.registerToNameServer(state);

                if (acknowledgment.getFlag() == RegistrationFlags.FORMAT_DISK) {
                    fileManagerService.formatNodeFiles();
                    state.setFree(state.getCapacity());
                }

                state.setName(acknowledgment.getNodeName());
                stateManager.writeState(state);

                this.registered = true;

            } catch (ServiceException | HttpClientErrorException | ResourceAccessException e) {
                registered = false;
                logger.error("Could not register to Name Server");
                logger.info("Trying again...");
            } finally {
                this.attemptCounter++;
            }

        }
    }
}
