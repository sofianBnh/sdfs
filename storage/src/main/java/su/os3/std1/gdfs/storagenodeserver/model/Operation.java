package su.os3.std1.gdfs.storagenodeserver.model;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import su.os3.std1.gdfs.storagenodeserver.config.SupportedOperations;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;

import java.sql.Timestamp;


public class Operation {

    private String digest;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Timestamp timestamp;
    private SupportedOperations operation;
    private long size;
    private String nodeAddress;
    private String path;

    public Operation() {
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    private Operation(
            SupportedOperations operation,
            String nodeAddress,
            String path,
            long size
    ) {
        this();
        this.operation = operation;
        this.nodeAddress = nodeAddress;
        this.path = path;
        this.size = size;
    }

    @JsonCreator
    public static Operation fromString(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Operation operation = objectMapper.readValue(json, Operation.class);
            assert operation.size != 0;
            assert operation.path != null;
            assert operation.digest != null;
            assert operation.operation != null;
            assert operation.nodeAddress != null;
            assert operation.timestamp != null;
            return operation;
        } catch (Exception e) {
            return null;
        }
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public SupportedOperations getOperation() {
        return operation;
    }

    public void setOperation(SupportedOperations operation) {
        this.operation = operation;
    }

    public String getNodeAddress() {
        return nodeAddress;
    }

    public void setNodeAddress(String nodeAddress) {
        this.nodeAddress = nodeAddress;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void sign() {

    }

    public void verify() throws ServiceException {
    }
}



