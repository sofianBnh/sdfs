package su.os3.std1.gdfs.storagenodeserver.service.files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import su.os3.std1.gdfs.storagenodeserver.config.FileStorageProperties;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicBoolean;

import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.*;

@Service
public class FileManagerService implements IFileManagerService {

    private final Path fileStorageLocation;
    private final Logger logger = LoggerFactory.getLogger(FileManagerService.class);

    @Autowired
    public FileManagerService(
            FileStorageProperties fileStorageProperties
    ) throws ServiceException {

        this.fileStorageLocation = Paths
                .get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {

            Files.createDirectories(this.fileStorageLocation);

        } catch (Exception ex) {
            throw new ServiceException(COULD_NOT_WRITE_DIRECTORY);
        }
    }

    @Override
    public String storeFile(MultipartFile file, String path) throws ServiceException {

        path = StringUtils.cleanPath(path.substring(1));

        try {

            Path targetLocation = this.fileStorageLocation.resolve(path);

            Path parent = targetLocation.getParent();

            if (!(Files.exists(parent) || new File(parent.toString()).mkdirs()))
                throw new ServiceException(COULD_NOT_WRITE_FILE);

            logger.info("Copying to storage " + file.getBytes().length);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return path;

        } catch (IOException ex) {
            throw new ServiceException(COULD_NOT_WRITE_FILE);
        }
    }

    public Resource loadFileAsResource(String path) throws ServiceException {

        try {
            path = StringUtils.cleanPath(path.substring(1));

            Path filePath = this.fileStorageLocation.resolve(path).normalize();

            Resource resource = new UrlResource(filePath.toUri());

            if (resource.exists())
                return resource;
            else
                throw new ServiceException(FILE_NOT_FOUND);

        } catch (MalformedURLException ex) {
            throw new ServiceException(INVALID_PATH);
        }
    }

    @Override
    public void delete(String path) throws ServiceException {

        path = StringUtils.cleanPath(path.substring(1));

        Path filePath = this.fileStorageLocation.resolve(path).normalize();

        File file = new File(filePath.toString());

        if (file.exists()) {

            boolean delete = file.delete();

            if (!delete)
                throw new ServiceException(COULD_NOT_DELETE_FILE);

        }

    }

    @Override
    public void formatNodeFiles() throws ServiceException {
        AtomicBoolean allDeleted = new AtomicBoolean(true);
        try {
            Files.walk(this.fileStorageLocation)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(file -> allDeleted.set(file.delete()));
        } catch (IOException e) {
            allDeleted.set(false);
        }

        if (!allDeleted.get())
            logger.error("Could not format disk");

    }


}

// https://www.callicoder.com/spring-boot-file-upload-download-rest-api-example/