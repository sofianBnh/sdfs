package su.os3.std1.gdfs.storagenodeserver.config;

public enum RegistrationFlags {
    KEEP_DATA, FORMAT_DISK
}
