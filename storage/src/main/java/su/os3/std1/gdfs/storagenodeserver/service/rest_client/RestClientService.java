package su.os3.std1.gdfs.storagenodeserver.service.rest_client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.ByteMultipart;
import su.os3.std1.gdfs.storagenodeserver.model.NodeStatus;
import su.os3.std1.gdfs.storagenodeserver.model.Operation;
import su.os3.std1.gdfs.storagenodeserver.model.RegistrationAcknowledgment;

import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.*;
import static su.os3.std1.gdfs.storagenodeserver.utils.CommonUtils.getCustomRestTemplate;
import static su.os3.std1.gdfs.storagenodeserver.utils.CommonUtils.getMasterNodeDownloadUrl;

@Service
public class RestClientService implements IRestClientService {

    private final RestTemplate restTemplate;
    private final String nameServerLink;

    @Autowired
    public RestClientService(
            @Value("${name-server.port}") int nameServerPort,
            @Value("${name-server.address}") String nameServerAddress

    ) throws Exception {
        this.restTemplate = getCustomRestTemplate();
        this.nameServerLink = String.format("%s://%s:%d", PROTOCOL, nameServerAddress, nameServerPort);
    }


    @Override
    public void notifyNameService(Operation operation) throws ServiceException {

        String notificationEndpoint = nameServerLink + NOTIFICATION_END_POINT;

        ResponseEntity<String> response = restTemplate.postForEntity(
                notificationEndpoint, operation, String.class
        );

        if (response == null || response.getStatusCode().isError())
            throw new ServiceException(UNABLE_TO_NOTIFY_SERVER);

    }

    @Override
    public RegistrationAcknowledgment registerToNameServer(NodeStatus current) throws ServiceException {

        final String registerEndpoint = nameServerLink + REGISTRATION_END_POINT;

        ResponseEntity<RegistrationAcknowledgment> response = restTemplate.postForEntity(
                registerEndpoint, current, RegistrationAcknowledgment.class
        );

        if (response.getStatusCode().is2xxSuccessful())
            return response.getBody();

        throw new ServiceException(UNABLE_TO_REGISTER);
    }

    @Override
    public MultipartFile pullFileFromNode(Operation operation) throws ServiceException {

        final String masterNodeDownloadUrl = getMasterNodeDownloadUrl(operation);

        final ResponseEntity<byte[]> response = restTemplate.postForEntity(
                masterNodeDownloadUrl
                , operation, byte[].class
        );

        if (response.getStatusCode().isError())
            throw new ServiceException(CANT_PULL_FILE);

        return new ByteMultipart(response.getBody(), operation.getPath());
    }

}
