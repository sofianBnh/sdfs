package su.os3.std1.gdfs.storagenodeserver.config;

import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.List;

import static su.os3.std1.gdfs.storagenodeserver.config.SupportedOperations.*;

public class ApplicationConstants {

    public static final MediaType FILES_TYPE = MediaType.APPLICATION_OCTET_STREAM;

    public static final List<SupportedOperations> EXECUTABLE_OPERATIONS = Arrays
            .asList(PULL, DELETE, DELETE_REPLICA);

    public static final String PROTOCOL = "https";
    public static final String REGISTRATION_END_POINT = "nodes/register";
    public static final String NOTIFICATION_END_POINT = "nodes/commit";
    public static final String DOWNLOAD_NODE_ENDPOINT = "storage/download";

    public static final int REGISTRATION_INTERVAL = 1000;
    public static final int REGISTRATION_RETRY_INTERVAL = 4 * REGISTRATION_INTERVAL;
    public static final int RETRY_ATTEMPTS = 4;


    // Errors


    public static final String INVALID_PATH = "INVALID_PATH";
    public static final String CANT_PULL_FILE = "CANT_PULL_FILE";
    public static final String FILE_NOT_FOUND = "FILE_NOT_FOUND";
    public static final String WRONG_OPERATION = "WRONG_OPERATION";
    public static final String UNABLE_TO_REGISTER = "UNABLE_TO_REGISTER";
    public static final String MALFORMED_OPERATION = "MALFORMED_OPERATION";
    public static final String INTEGRITY_VIOLATION = "INTEGRITY_VIOLATION";
    public static final String COULD_NOT_WRITE_FILE = "COULD_NOT_WRITE_FILE";
    public static final String COULD_NOT_READ_STATE = "COULD_NOT_READ_STATE";
    public static final String COULD_NOT_WRITE_STATE = "COULD_NOT_WRITE_STATE";
    public static final String COULD_NOT_DELETE_FILE = "COULD_NOT_DELETE_FILE";
    public static final String UNABLE_TO_NOTIFY_SERVER = "UNABLE_TO_NOTIFY_SERVER";
    public static final String COULD_NOT_WRITE_DIRECTORY = "COULD_NOT_WRITE_DIRECTORY";
    public static final String FILE_EXCEEDS_SPECIFIED_SIZE = "FILE_EXCEEDS_SPECIFIED_SIZE";
}
