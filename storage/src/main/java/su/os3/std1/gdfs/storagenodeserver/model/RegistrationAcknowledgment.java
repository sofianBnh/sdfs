package su.os3.std1.gdfs.storagenodeserver.model;


import su.os3.std1.gdfs.storagenodeserver.config.RegistrationFlags;

public class RegistrationAcknowledgment {

    private String nodeName;
    private RegistrationFlags flag;

    public RegistrationAcknowledgment(String nodeName, RegistrationFlags flag) {
        this.nodeName = nodeName;
        this.flag = flag;
    }

    public RegistrationAcknowledgment() {
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public RegistrationFlags getFlag() {
        return flag;
    }

    public void setFlag(RegistrationFlags flag) {
        this.flag = flag;
    }

}
