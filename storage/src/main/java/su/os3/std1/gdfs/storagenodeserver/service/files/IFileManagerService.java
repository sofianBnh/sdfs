package su.os3.std1.gdfs.storagenodeserver.service.files;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;

public interface IFileManagerService {

    String storeFile(MultipartFile fieStream, String name) throws ServiceException;

    Resource loadFileAsResource(String path) throws ServiceException;

    void delete(String path) throws ServiceException;

    void formatNodeFiles() throws ServiceException;

}
