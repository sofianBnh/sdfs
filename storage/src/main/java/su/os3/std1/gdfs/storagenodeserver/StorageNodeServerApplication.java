package su.os3.std1.gdfs.storagenodeserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;
import su.os3.std1.gdfs.storagenodeserver.config.FileStorageProperties;

@EnableScheduling
@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class StorageNodeServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StorageNodeServerApplication.class, args);
    }

}
