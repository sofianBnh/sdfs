package su.os3.std1.gdfs.storagenodeserver.utils;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import su.os3.std1.gdfs.storagenodeserver.model.Operation;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.DOWNLOAD_NODE_ENDPOINT;
import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.PROTOCOL;

public class CommonUtils {

    public static String formatNodeAddress(String ip, int port) {
        return String.format("%s:%d", ip, port);
    }

    public static String formatNodeUrl(String ip, int port) {
        return String.format("%s://%s", PROTOCOL, formatNodeAddress(ip, port));
    }

    public static String getMasterNodeDownloadUrl(Operation operation) {
        return PROTOCOL + "://" + operation.getNodeAddress()
                + "/" + DOWNLOAD_NODE_ENDPOINT;
    }

    public static RestTemplate getCustomRestTemplate() throws
            NoSuchAlgorithmException, KeyManagementException, KeyStoreException {

        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(
                sslContext, new NoopHostnameVerifier());

        CloseableHttpClient httpClient = HttpClients.custom()
                .setRetryHandler(new DefaultHttpRequestRetryHandler(
                        0,
                        false))
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);

        return new RestTemplate(requestFactory);
    }
}
