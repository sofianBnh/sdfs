package su.os3.std1.gdfs.storagenodeserver.service.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.NodeStatus;
import su.os3.std1.gdfs.storagenodeserver.model.Operation;
import su.os3.std1.gdfs.storagenodeserver.service.files.IFileManagerService;
import su.os3.std1.gdfs.storagenodeserver.service.rest_client.IRestClientService;
import su.os3.std1.gdfs.storagenodeserver.service.state.IStateManager;

import java.beans.Transient;

import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.FILE_EXCEEDS_SPECIFIED_SIZE;
import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.WRONG_OPERATION;

@Service
public class StorageStateManager implements IStorageStateManager {

    private final IFileManagerService fileManagerService;
    private final IRestClientService restClientService;
    private final IStateManager stateManager;

    @Autowired
    public StorageStateManager(
            IFileManagerService fileManagerService,
            IRestClientService restClientService,
            IStateManager stateManager) {
        this.fileManagerService = fileManagerService;
        this.restClientService = restClientService;
        this.stateManager = stateManager;
    }

    @Override
    public Resource download(Operation operation) throws ServiceException {
        String path = operation.getPath();
        return fileManagerService.loadFileAsResource(path);
    }

    @Override
    public synchronized void upload(Operation operation, MultipartFile file) throws ServiceException {

        if (operation.getSize() != file.getSize())
            throw new ServiceException(FILE_EXCEEDS_SPECIFIED_SIZE);

        String name = operation.getPath();
        fileManagerService.storeFile(file, name);

        try {

            restClientService.notifyNameService(operation);

            NodeStatus nodeStatus = stateManager.readState();
            nodeStatus.setFree(nodeStatus.getFree() - file.getSize());
            stateManager.writeState(nodeStatus);

        } catch (Exception e) {
            fileManagerService.delete(operation.getPath());
            throw e;
        }

    }

    @Override
    public void executeOperation(Operation operation) throws ServiceException {

        NodeStatus nodeStatus = stateManager.readState();

        switch (operation.getOperation()) {
            case DELETE:
                fileManagerService.delete(operation.getPath());
                restClientService.notifyNameService(operation);
                nodeStatus.setFree(nodeStatus.getFree() + operation.getSize());
                return;

            case DELETE_REPLICA:
                fileManagerService.delete(operation.getPath());
                restClientService.notifyNameService(operation);
                nodeStatus.setFree(nodeStatus.getFree() + operation.getSize());
                return;

            case PULL:
                MultipartFile file = restClientService.pullFileFromNode(operation);
                upload(operation, file);
                return;
            default:
                throw new ServiceException(WRONG_OPERATION);
        }

    }


}
