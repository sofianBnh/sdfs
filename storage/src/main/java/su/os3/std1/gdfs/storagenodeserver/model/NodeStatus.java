package su.os3.std1.gdfs.storagenodeserver.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class NodeStatus {

    private long capacity;
    private long free;
    private String name;
    private int port;

    public NodeStatus() {
    }

    public NodeStatus(long capacity, long free, String name, int port) {
        this.capacity = capacity;
        this.free = free;
        this.name = name;
        this.port = port;
    }

    public static NodeStatus fromJson(String stateString) throws IOException {

        return new ObjectMapper().readValue(stateString, NodeStatus.class);

    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public long getFree() {
        return free;
    }

    public void setFree(long free) {
        this.free = free;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String toJsonString() throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(this);
    }
}
