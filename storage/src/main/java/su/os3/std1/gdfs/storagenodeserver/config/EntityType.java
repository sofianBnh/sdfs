package su.os3.std1.gdfs.storagenodeserver.config;

public enum EntityType {
    FILE,
    DIRECTORY
}
