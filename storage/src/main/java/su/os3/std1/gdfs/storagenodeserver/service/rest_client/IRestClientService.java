package su.os3.std1.gdfs.storagenodeserver.service.rest_client;

import org.springframework.web.multipart.MultipartFile;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.NodeStatus;
import su.os3.std1.gdfs.storagenodeserver.model.Operation;
import su.os3.std1.gdfs.storagenodeserver.model.RegistrationAcknowledgment;

public interface IRestClientService {

    void notifyNameService(Operation operation) throws ServiceException;

    RegistrationAcknowledgment registerToNameServer(NodeStatus current) throws ServiceException;

    MultipartFile pullFileFromNode(Operation operation) throws ServiceException;

}
