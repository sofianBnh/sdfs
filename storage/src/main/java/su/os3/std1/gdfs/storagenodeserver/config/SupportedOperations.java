package su.os3.std1.gdfs.storagenodeserver.config;

public enum SupportedOperations {
    READ, WRITE, DELETE, DELETE_REPLICA, PULL,
}
