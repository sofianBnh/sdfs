package su.os3.std1.gdfs.storagenodeserver.service.state;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import su.os3.std1.gdfs.storagenodeserver.config.FileStorageProperties;
import su.os3.std1.gdfs.storagenodeserver.error.ServiceException;
import su.os3.std1.gdfs.storagenodeserver.model.NodeStatus;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.COULD_NOT_READ_STATE;
import static su.os3.std1.gdfs.storagenodeserver.config.ApplicationConstants.COULD_NOT_WRITE_STATE;

@Service
public class StateManager implements IStateManager {

    private final Path stateLocation;
    private final String stateFile;
    private final long capacity;
    private final int currentPort;

    @Autowired
    public StateManager(
            FileStorageProperties fileStorageProperties,
            @Value("${node.capacity}") long capacity,
            @Value("${server.port}") int currentPort
    ) {
        this.stateLocation = Paths
                .get(fileStorageProperties.getStateDir())
                .toAbsolutePath().normalize();
        this.stateFile = fileStorageProperties.getStateFile();
        this.capacity = capacity;
        this.currentPort = currentPort;
    }

    @Override
    public void writeState(NodeStatus current) throws ServiceException {

        try {
            final String statusJson = current.toJsonString();
            final Path targetLocation = this.stateLocation.resolve(stateFile);


            File parent = targetLocation.getParent().toFile();

            parent.mkdirs();

            if (!parent.exists())
                throw new ServiceException(COULD_NOT_WRITE_STATE);

            Files.write(targetLocation, statusJson.getBytes());
        } catch (IOException e) {
            throw new ServiceException(COULD_NOT_WRITE_STATE);
        }
    }

    @Override
    public NodeStatus readState() throws ServiceException {

        try {
            final Path targetLocation = this.stateLocation.resolve(stateFile);
            final String stateString = String.join("", Files.readAllLines(targetLocation));
            return NodeStatus.fromJson(stateString);
        } catch (IOException e) {
            throw new ServiceException(COULD_NOT_READ_STATE);
        }
    }

    @Override
    public boolean isStateAvailable() throws ServiceException {
        final Path targetLocation = this.stateLocation.resolve(stateFile);
        return Files.exists(targetLocation);
    }

    @Override
    public void writeInitialState() throws ServiceException {

        if (isStateAvailable())
            return;

        NodeStatus initialStatus = new NodeStatus(
                capacity,
                capacity,
                "",
                currentPort
        );

        writeState(initialStatus);
    }

    @Override
    public void formatNodeState() throws ServiceException {
        final Path targetLocation = this.stateLocation.resolve(stateFile);

        File parent = targetLocation.toFile();
        parent.delete();

    }
}
