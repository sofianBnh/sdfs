package su.std1.gdfs.nameserver.model.dto;

public class NodeStatus {

    private long capacity;
    private long free;
    private String name;
    private int port;

    public NodeStatus() {
    }

    public NodeStatus(long capacity, long free, String name, int port) {
        this.capacity = capacity;
        this.free = free;
        this.name = name;
        this.port = port;
    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public long getFree() {
        return free;
    }

    public void setFree(long free) {
        this.free = free;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
