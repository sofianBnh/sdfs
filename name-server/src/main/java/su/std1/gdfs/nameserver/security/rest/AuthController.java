package su.std1.gdfs.nameserver.security.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.dto.Login;
import su.std1.gdfs.nameserver.security.service.AuthService;

@Controller
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("auth")
    public ResponseEntity<?> getToken(@RequestBody Login login) throws ServiceException {
        return ResponseEntity.ok(authService.generateToken(login));
    }


}
