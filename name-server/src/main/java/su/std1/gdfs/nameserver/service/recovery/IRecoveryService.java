package su.std1.gdfs.nameserver.service.recovery;

import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.StorageNodePair;

import java.util.List;

public interface IRecoveryService {

    void  handleWidowPairs() throws ServiceException;

    List<StorageNodePair> findResurrectedPairs() throws ServiceException;

    void handleResurrectedPairs(List<StorageNodePair> widowPairs) throws ServiceException;

}
