package su.std1.gdfs.nameserver.model.data;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class StorageNode {

    @Id
    private ObjectId _id;

    private String address;
    private int port;
    private long free;
    private long capacity;
    private boolean alive;

    public StorageNode() {
        super();
        this.alive = true;
    }

    public StorageNode(String address, int port, long free, long capacity) {
        this();
        this.address = address;
        this.port = port;
        this.free = free;
        this.capacity = capacity;
    }

    public StorageNode(ObjectId _id, String address, int port, long free, long capacity, boolean alive) {
        this(address, port, free, capacity);
        this._id = _id;
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public long getFree() {
        return free;
    }

    public void setFree(long free) {
        this.free = free;
    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StorageNode node = (StorageNode) o;
        return Objects.equals(_id.toHexString(), node._id.toHexString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(_id.toHexString());
    }
}
