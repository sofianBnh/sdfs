package su.std1.gdfs.nameserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import su.std1.gdfs.nameserver.model.data.Directory;
import su.std1.gdfs.nameserver.security.model.User;
import su.std1.gdfs.nameserver.repository.DirectoryRepository;
import su.std1.gdfs.nameserver.repository.UserRepository;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.PATH_DELIMITER;

@Configuration
public class Starter implements ApplicationRunner {


    private final DirectoryRepository directoryRepository;
    private final UserRepository userRepository;

    @Autowired
    public Starter(
            UserRepository userRepository,
            DirectoryRepository directoryRepository
    ) {
        this.directoryRepository = directoryRepository;
        this.userRepository = userRepository;
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (!directoryRepository.existsByDirectoryPath(PATH_DELIMITER)) {
            Directory rootInstance = new Directory(PATH_DELIMITER);
            directoryRepository.save(rootInstance);
        }

        String sdfsclient = "sdfsclient";
        User user = new User(sdfsclient, "VerySecret");


        if (!userRepository.findByUsername(sdfsclient).isPresent())
            userRepository.save(user);


    }

}
