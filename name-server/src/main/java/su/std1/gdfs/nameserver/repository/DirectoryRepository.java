package su.std1.gdfs.nameserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import su.std1.gdfs.nameserver.model.data.Directory;

import java.util.Optional;

@Repository
public interface DirectoryRepository extends MongoRepository<Directory, String> {
    Optional<Directory> findByDirectoryPath(String path);

    boolean existsByDirectoryPath(String path);
}
