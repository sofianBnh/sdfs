package su.std1.gdfs.nameserver.config;

public enum RegistrationFlags {
    KEEP_DATA, FORMAT_DISK
}
