package su.std1.gdfs.nameserver.service.recovery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.File;
import su.std1.gdfs.nameserver.model.data.StorageNode;
import su.std1.gdfs.nameserver.model.data.StorageNodePair;
import su.std1.gdfs.nameserver.repository.StorageNodePairRepository;
import su.std1.gdfs.nameserver.repository.StorageNodeRepository;
import su.std1.gdfs.nameserver.service.storage.IStorageService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecoveryService implements IRecoveryService {


    private final IStorageService storageService;
    private final StorageNodeRepository storageNodeRepository;
    private final StorageNodePairRepository storageNodePairRepository;

    @Autowired
    public RecoveryService(
            IStorageService storageService,
            StorageNodeRepository storageNodeRepository,
            StorageNodePairRepository storageNodePairRepository
    ) {
        this.storageService = storageService;
        this.storageNodeRepository = storageNodeRepository;
        this.storageNodePairRepository = storageNodePairRepository;
    }

    @Override
    public synchronized void handleWidowPairs() throws ServiceException {

        storageNodePairRepository.findAll().forEach(

                pair -> {

                    StorageNode master = pair.getMaster();
                    StorageNode slave = pair.getSlave();


                    if (master != null && !master.isAlive()) {

                        if (slave == null) {
                            storageService.changeAllFilesState(pair.getFiles(), false);

                        } else {

                            if (slave.isAlive()) {

                                master.setFree(master.getCapacity());
                                storageNodeRepository.save(master);
                                pair.flipServers();
                                pair.setSlave(null);


                            } else {
                                storageService.changeAllFilesState(pair.getFiles(), false);

                                pair.setSlave(null);
                                slave.setFree(slave.getCapacity());
                                storageNodeRepository.save(slave);

                            }

                            storageNodePairRepository.save(pair);
                        }

                    } else {
                        if (slave != null && !slave.isAlive()) {

                            pair.setSlave(null);
                            slave.setFree(slave.getCapacity());
                            storageNodeRepository.save(slave);
                            storageNodePairRepository.save(pair);

                        }
                    }
                }
        );
    }

    @Override
    public synchronized List<StorageNodePair> findResurrectedPairs() throws ServiceException {

        List<StorageNodePair> pairsWithAliveNodes = storageNodePairRepository.findAll();

        return pairsWithAliveNodes
                .stream()
                .filter(
                        pair ->
                                pair.getMaster().isAlive()
                                        && pair.getFiles().size() != 0
                                        && pair.getFiles().stream().noneMatch(File::isAvailable)
                ).collect(Collectors.toList());

    }

    @Override
    public synchronized void handleResurrectedPairs(List<StorageNodePair> widowPairs) throws ServiceException {
        widowPairs.forEach(pair -> storageService.changeAllFilesState(pair.getFiles(), true));
    }


}
