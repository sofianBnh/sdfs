package su.std1.gdfs.nameserver.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import su.std1.gdfs.nameserver.model.data.File;

import java.util.Optional;

@Repository
public interface FileRepository extends MongoRepository<File, String> {

    Optional<File> findByFilePath(String path);

    boolean existsByFilePath(String path);

}
