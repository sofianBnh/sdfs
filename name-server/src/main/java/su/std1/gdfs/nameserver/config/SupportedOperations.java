package su.std1.gdfs.nameserver.config;

public enum SupportedOperations {
    READ, WRITE, DELETE, DELETE_REPLICA, PULL
}
