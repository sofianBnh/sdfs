package su.std1.gdfs.nameserver.service.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.std1.gdfs.nameserver.config.EntityType;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.Directory;
import su.std1.gdfs.nameserver.model.data.File;
import su.std1.gdfs.nameserver.model.dto.EntityProperties;
import su.std1.gdfs.nameserver.repository.DirectoryRepository;
import su.std1.gdfs.nameserver.repository.FileRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.DIRECTORY_SIZE;
import static su.std1.gdfs.nameserver.config.ApplicationConstants.NO_SUCH_FILE_OR_DIRECTORY;
import static su.std1.gdfs.nameserver.utils.CommonUtils.*;

@Service
public class StorageService implements IStorageService {


    private final FileRepository fileRepository;
    private final DirectoryRepository directoryRepository;


    @Autowired
    public StorageService(
            FileRepository fileRepository,
            DirectoryRepository directoryRepository
    ) {
        this.fileRepository = fileRepository;
        this.directoryRepository = directoryRepository;
    }


    @Override
    public List<EntityProperties> listItemsInDirectory(String path) throws ServiceException {

        path = normalizeDirectoryPath(path);

        Directory directory = retrieveDirectory(path);

        final List<File> files = directory.getFiles();
        final List<Directory> directories = directory.getDirectories();

        List<EntityProperties> properties = files
                .stream()
                .map(
                        file -> new EntityProperties(
                                EntityType.FILE,
                                file.getSize(),
                                file.getFilePath()
                        )
                ).collect(Collectors.toList());

        properties.addAll(directories
                .stream()
                .map(subDirectory -> new EntityProperties(
                        EntityType.DIRECTORY,
                        DIRECTORY_SIZE,
                        subDirectory.getDirectoryPath()
                )).collect(Collectors.toList())
        );

        properties.sort(Comparator.comparing(EntityProperties::getPath));

        return properties;
    }

    @Override
    public EntityProperties getProperties(String path) throws ServiceException {

        EntityType type = getType(path);

        if (type == EntityType.DIRECTORY) {

            path = normalizeDirectoryPath(path);

            Optional<Directory> directory = directoryRepository.findByDirectoryPath(path);

            if (directory.isPresent())
                return new EntityProperties(
                        EntityType.DIRECTORY,
                        DIRECTORY_SIZE,
                        directory.get().getDirectoryPath());

        } else {

            path = normalizeFilePath(path);

            Optional<File> file = fileRepository.findByFilePath(path);

            if (file.isPresent())
                return new EntityProperties(
                        EntityType.FILE,
                        file.get().getSize(),
                        file.get().getFilePath()
                );

        }

        throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);
    }

    @Override
    public boolean entityExists(String path) {
        return directoryRepository.existsByDirectoryPath(path)
                || fileRepository.existsByFilePath(path);
    }

    @Override
    public boolean parentDirectoryExists(String path) throws ServiceException {
        String parentDirPath = getParentDirPath(path);
        return directoryRepository.existsByDirectoryPath(parentDirPath);
    }

    @Override
    public EntityType getType(String path) throws ServiceException {

        if (isLastCharacterSlash(path)) {

            if (directoryRepository.existsByDirectoryPath(path))
                return EntityType.DIRECTORY;
            else if (fileRepository.existsByFilePath(normalizeFilePath(path)))
                return EntityType.FILE;

        } else {

            if (directoryRepository.existsByDirectoryPath(normalizeDirectoryPath(path)))
                return EntityType.DIRECTORY;
            else if (fileRepository.existsByFilePath(path))
                return EntityType.FILE;

        }

        throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);
    }

    @Override
    public File makeFile(File file) throws ServiceException {

        String parentDirPath = getParentDirPath(file.getFilePath());

        Optional<Directory> parent = directoryRepository.findByDirectoryPath(parentDirPath);

        if (!parent.isPresent())
            throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);


        if (!hasChild(parent.get(), file.getFilePath()))
            parent.get().addFile(file);

        file.setParent(parent.get());

        File saved = fileRepository.save(file);
        directoryRepository.save(parent.get());
        return saved;
    }

    @Override
    public Directory makeDirectory(Directory directory) throws ServiceException {

        String parentDirPath = getParentDirPath(directory.getDirectoryPath());

        Optional<Directory> parent = directoryRepository.findByDirectoryPath(parentDirPath);

        if (!parent.isPresent())
            throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);

        if (!hasChild(parent.get(), directory.getDirectoryPath()))
            parent.get().addDirectory(directory);

        directory.setParent(parent.get());

        Directory saved = directoryRepository.save(directory);
        directoryRepository.save(parent.get());
        return saved;
    }

    @Override
    public File retrieveFile(String path) throws ServiceException {

        Optional<File> file = fileRepository.findByFilePath(path);

        if (!file.isPresent())
            throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);

        return file.get();
    }

    @Override
    public Directory retrieveDirectory(String path) throws ServiceException {

        Optional<Directory> directory = directoryRepository.findByDirectoryPath(path);

        if (!directory.isPresent())
            throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);

        return directory.get();
    }

    @Override
    public void deleteFile(File file) throws ServiceException {

        Directory parent = file.getParent();

        if (parent == null)
            throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);

        fileRepository.delete(file);

        parent.removeFile(file);
        directoryRepository.save(parent);
    }

    @Override
    public void deleteDirectory(Directory directory) throws ServiceException {

        Directory parent = directory.getParent();

        if (parent == null)
            throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);

        directoryRepository.delete(directory);

        parent.removeDirectory(directory);
        directoryRepository.save(parent);
    }


    @Override
    public void changeAllFilesState(List<File> files, boolean available) {

        files.forEach(file -> {
            file.setAvailable(available);
            try {
                makeFile(file);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        });


    }

    private boolean hasChild(Directory parent, String currentPath) {
        return parent
                .getFiles()
                .stream()
                .anyMatch(
                        existingFiles -> currentPath
                                .equals(existingFiles.getFilePath())
                );
    }


}
