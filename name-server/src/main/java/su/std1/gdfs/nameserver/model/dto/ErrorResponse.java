package su.std1.gdfs.nameserver.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.Timestamp;

public class ErrorResponse {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Timestamp timestamp;
    private String message;


    public ErrorResponse() {
    }

    public ErrorResponse(Timestamp timestamp, String message) {
        this.timestamp = timestamp;
        this.message = message;
    }

    public ErrorResponse(String message) {
        this.message = message;
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    @JsonCreator
    public static ErrorResponse fromString(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ErrorResponse response = objectMapper.readValue(json, ErrorResponse.class);
            assert response.message != null;
            assert response.timestamp != null;
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp.toString();
    }
}
