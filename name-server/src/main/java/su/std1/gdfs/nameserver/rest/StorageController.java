package su.std1.gdfs.nameserver.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.Operation;
import su.std1.gdfs.nameserver.model.dto.EntityProperties;
import su.std1.gdfs.nameserver.security.service.AuthService;
import su.std1.gdfs.nameserver.service.entity.IEntityManagementService;

import javax.websocket.server.PathParam;
import java.util.List;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.*;
import static su.std1.gdfs.nameserver.utils.CommonUtils.checkPathBasics;

@Controller
public class StorageController {

    private final IEntityManagementService entityManagementService;
    private final AuthService authService;

    @Autowired
    public StorageController(IEntityManagementService entityManagementService,
                             AuthService authService) {
        this.entityManagementService = entityManagementService;
        this.authService = authService;
    }

    @GetMapping("storage")
    public ResponseEntity<?> read(@PathParam("path") String path) throws ServiceException {
        checkPathBasics(path);
        path = path.trim();
        Operation read = entityManagementService.read(path);
        read.sign();
        return ResponseEntity.ok(read);
    }

    @PostMapping("storage")
    public ResponseEntity<?> write(@RequestBody EntityProperties metaData) throws ServiceException {
        Operation write = entityManagementService.write(metaData);
        write.sign();
        return ResponseEntity.ok(write);
    }

    @DeleteMapping("storage")
    public ResponseEntity<?> delete(@PathParam("path") String path, @RequestHeader HttpHeaders headers)
            throws ServiceException {

        List<String> header = headers.get(HttpHeaders.AUTHORIZATION);
        if (header == null || header.size() == 0)
            throw new ServiceException(BAD_CREDENTIALS);

        authService.verifyToken(header.get(0));

        checkPathBasics(path);
        path = path.trim();
        if (path.equals(PATH_DELIMITER)) throw new ServiceException(ROOT_CANT_BE_DELETED);
        entityManagementService.delete(path);
        return ResponseEntity.ok("");
    }


    @GetMapping("properties")
    public ResponseEntity<?> properties(@PathParam("path") String path) throws ServiceException {
        checkPathBasics(path);
        path = path.trim();
        return ResponseEntity.ok(entityManagementService.getPropertiesWithNode(path));
    }

    @GetMapping("list")
    public ResponseEntity<?> listDirectories(@PathParam("path") String path) throws ServiceException {
        checkPathBasics(path);
        path = path.trim();
        return ResponseEntity.ok(entityManagementService.listItemsWithNodes(path));
    }


}
