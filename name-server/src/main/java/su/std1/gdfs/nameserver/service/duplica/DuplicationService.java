package su.std1.gdfs.nameserver.service.duplica;

import org.springframework.stereotype.Service;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.File;
import su.std1.gdfs.nameserver.model.data.StorageNode;
import su.std1.gdfs.nameserver.model.data.StorageNodePair;
import su.std1.gdfs.nameserver.repository.StorageNodePairRepository;
import su.std1.gdfs.nameserver.repository.StorageNodeRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.*;

@Service
public class DuplicationService implements IDuplicationService {


    private final StorageNodeRepository storageNodeRepository;
    private final StorageNodePairRepository storageNodePairRepository;

    public DuplicationService(
            StorageNodeRepository storageNodeRepository,
            StorageNodePairRepository storageNodePairRepository
    ) {
        this.storageNodeRepository = storageNodeRepository;
        this.storageNodePairRepository = storageNodePairRepository;
    }

    // Logic
    //==============================================================================================

    @Override
    public StorageNodePair findNodePairForFileSize(long size) throws ServiceException {

        List<StorageNodePair> candidates = storageNodePairRepository.findAll();

        candidates = candidates.stream().filter(

                pair -> pair.getMaster().getFree() >= size && pair.getMaster().isAlive()

        ).sorted(Comparator.comparing(
                storageNodePair -> -storageNodePair
                        .getMaster()
                        .getFree()
        )).
                collect(Collectors.toList());

        if (candidates.size() == 0)
            throw new ServiceException(INSUFFICIENT_STORAGE);

        return candidates.get(0);

    }


    @Override
    public synchronized boolean buildPairOrSlaveAvailable(StorageNode newNode) throws ServiceException {

        final List<StorageNodePair> allAlivePairs = storageNodePairRepository.findAll()
                .stream()
                .filter(pair -> pair.getMaster().isAlive())
                .collect(Collectors.toList());

        final List<StorageNode> freeNodes = getFreeNodes();

        if (freeNodes.size() > allAlivePairs.size()) {
            StorageNodePair nodePair = new StorageNodePair(newNode);
            storageNodePairRepository.save(nodePair);
            return false;
        }

        return true;
    }

    // Getters
    //==============================================================================================

    @Override
    public StorageNode getReadNode(File file) throws ServiceException {

        StorageNodePair storageNodePair = getStorageNodePair(file);
        StorageNode slave = storageNodePair.getSlave();

        if (slave == null)
            return storageNodePair.getMaster();

        return slave;
    }

    @Override
    public StorageNode getWriteNode(File file) throws ServiceException {
        return getStorageNodePair(file).getMaster();
    }

    @Override
    public StorageNode getSlave(File file) throws ServiceException {
        StorageNodePair storageNodePair = getStorageNodePair(file);
        return storageNodePair.getSlave();
    }

    @Override
    public StorageNodePair getStorageNodePair(File file) throws ServiceException {

        List<StorageNodePair> responsiblePairs = storageNodePairRepository
                .findAll();
        responsiblePairs = responsiblePairs.
                stream()
                .filter(pair -> pair.getFiles().contains(file))
                .collect(Collectors.toList());

        if (responsiblePairs.size() > 1)
            throw new ServiceException(MORE_PAIRS_THAN_EXPECTED);

        if (responsiblePairs.size() == 0)
            throw new ServiceException(COULD_NOT_FIND_NODE_PAIR);

        return responsiblePairs.get(0);
    }

    @Override
    public List<StorageNode> getFreeNodes() {

        List<StorageNode> all = storageNodeRepository.findAllByAlive(true);

        List<StorageNode> pairedNodesIds = new ArrayList<>();

        storageNodePairRepository.findAll().forEach(
                node -> {
                    pairedNodesIds.add(node.getMaster());
                    StorageNode slave = node.getSlave();
                    if (slave != null) pairedNodesIds.add(slave);
                }
        );

        all.removeAll(pairedNodesIds);
        return all;
    }

    @Override
    public List<StorageNodePair> getSinglePairs() {

        return storageNodePairRepository
                .findAll()
                .stream()
                .filter(
                        pair -> pair.getSlave() == null || !pair.getSlave().isAlive()
                )
                .collect(Collectors.toList());
    }


    @Override
    public List<StorageNodePair> getPairByNode(StorageNode node) throws ServiceException {
        return storageNodePairRepository.findAll()
                .stream()
                .filter(
                        pair -> {
                            if (pair.getMaster().equals(node))
                                return true;
                            return pair.getSlave() != null && pair.getSlave().equals(node);
                        }
                ).collect(Collectors.toList());
    }

    // Setters
    //==============================================================================================

    @Override
    public void setSlave(StorageNode slave, StorageNodePair pair) {
        pair.setSlave(slave);
        storageNodePairRepository.save(pair);
    }

    @Override
    public void addFile(File file, StorageNodePair pair) throws ServiceException {
        pair.addFile(file);
        storageNodePairRepository.save(pair);
    }

    @Override
    public void deleteFile(File file) throws ServiceException {
        StorageNodePair pair = getStorageNodePair(file);
        pair.removeFile(file);
        storageNodePairRepository.save(pair);
    }

}
