package su.std1.gdfs.nameserver.service.duplica;

import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.File;
import su.std1.gdfs.nameserver.model.data.StorageNode;
import su.std1.gdfs.nameserver.model.data.StorageNodePair;

import java.util.List;

public interface IDuplicationService {

    void addFile(File file, StorageNodePair pair) throws ServiceException;

    StorageNodePair findNodePairForFileSize(long size) throws ServiceException;

    StorageNode getReadNode(File file) throws ServiceException;

    StorageNode getWriteNode(File file) throws ServiceException;

    List<StorageNodePair> getPairByNode(StorageNode node) throws ServiceException;

    boolean buildPairOrSlaveAvailable(StorageNode newNode) throws ServiceException;

    StorageNode getSlave(File file) throws ServiceException;

    void setSlave(StorageNode slave, StorageNodePair pair);

    List<StorageNode> getFreeNodes();

    List<StorageNodePair> getSinglePairs();

    StorageNodePair getStorageNodePair(File file) throws ServiceException;

    void deleteFile(File file) throws ServiceException;
}
