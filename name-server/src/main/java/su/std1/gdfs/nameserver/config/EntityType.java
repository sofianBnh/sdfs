package su.std1.gdfs.nameserver.config;

public enum EntityType {
    FILE,
    DIRECTORY
}
