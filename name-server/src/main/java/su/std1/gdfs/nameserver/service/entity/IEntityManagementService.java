package su.std1.gdfs.nameserver.service.entity;

import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.Operation;
import su.std1.gdfs.nameserver.model.dto.EntityProperties;

import java.util.List;

public interface IEntityManagementService {

    Operation read(String path) throws ServiceException;

    Operation write(EntityProperties metaData) throws ServiceException;

    void delete(String path) throws ServiceException;

    List<EntityProperties> listItemsWithNodes(String path) throws ServiceException;

    EntityProperties getPropertiesWithNode(String path) throws ServiceException;

    void commitOperation(Operation operation) throws ServiceException;

}
