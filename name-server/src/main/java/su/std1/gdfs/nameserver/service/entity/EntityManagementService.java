package su.std1.gdfs.nameserver.service.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import su.std1.gdfs.nameserver.config.SupportedOperations;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.*;
import su.std1.gdfs.nameserver.model.dto.EntityProperties;
import su.std1.gdfs.nameserver.repository.OperationRepository;
import su.std1.gdfs.nameserver.service.duplica.IDuplicationService;
import su.std1.gdfs.nameserver.service.nodes.INodeManagementService;
import su.std1.gdfs.nameserver.service.storage.IStorageService;

import java.util.ArrayList;
import java.util.List;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.*;
import static su.std1.gdfs.nameserver.config.EntityType.FILE;
import static su.std1.gdfs.nameserver.model.data.Operation.OperationBuilder.anOperation;
import static su.std1.gdfs.nameserver.utils.CommonUtils.*;

@Service
public class EntityManagementService implements IEntityManagementService {

    private final IStorageService storageService;
    private final IDuplicationService duplicationService;
    private final OperationRepository operationRepository;
    private final INodeManagementService nodeManagementService;


    @Autowired
    public EntityManagementService(
            IStorageService storageService,
            IDuplicationService duplicationService,
            OperationRepository operationRepository,
            INodeManagementService nodeManagementService
    ) {
        this.storageService = storageService;
        this.duplicationService = duplicationService;
        this.operationRepository = operationRepository;
        this.nodeManagementService = nodeManagementService;
    }


    // Read
    //=====================================================================================

    @Override
    public Operation read(String path) throws ServiceException {

        path = normalizeFilePath(path);

        if (storageService.getType(path) != FILE)
            throw new ServiceException(CANT_READ_DIRECTORY);

        final File file = storageService.retrieveFile(path);

        checkFileState(file);

        final StorageNode slave = duplicationService.getReadNode(file);

        final String nodeAddress = formatNodeAddress(slave.getAddress()
                , slave.getPort());

        Operation operation = anOperation()
                .withOperation(SupportedOperations.READ)
                .withNodeAddress(nodeAddress)
                .withPath(path)
                .withSize(file.getSize())
                .build();

        operationRepository.save(operation);
        return operation;

    }


    // Delete
    //=====================================================================================


    @Override
    public void delete(String path) throws ServiceException {

        if (storageService.getType(path) == FILE)
            deleteFile(path);
        else
            deleteDirectory(path);
    }

    private void deleteFile(String path) throws ServiceException {


        path = normalizeFilePath(path);
        File file = storageService.retrieveFile(path);

        checkFileState(file);

        StorageNode master = duplicationService.getWriteNode(file);

        file.setWritten(false);
        storageService.makeFile(file);

        Operation deleteOperation = anOperation().withPath(path)
                .withOperation(SupportedOperations.DELETE)
                .withSize(file.getSize())
                .build();

        try {
            nodeManagementService.sendOperation(deleteOperation, master);
        } catch (HttpClientErrorException e) {
            storageService.deleteFile(file);

        }

    }

    private void checkFileState(File file) throws ServiceException {

        if (!file.isWritten())
            throw new ServiceException(FILE_IN_USE);

        if (!file.isAvailable())
            throw new ServiceException(FILE_IS_UNAVAILABLE);
    }

    private void deleteDirectory(String path) throws ServiceException {

        path = normalizeDirectoryPath(path);

        Directory directory = storageService.retrieveDirectory(path);

        if (directory.getDirectories().size() != 0 || directory.getFiles().size() != 0)
            throw new ServiceException(DIRECTORY_IS_NOT_EMPTY);

        storageService.deleteDirectory(directory);
    }


    // Write
    //=====================================================================================


    @Override
    public Operation write(EntityProperties metaData) throws ServiceException {

        String path = metaData.getPath();
        checkPathBasics(path);

        if (!storageService.parentDirectoryExists(path))
            throw new ServiceException(NO_SUCH_FILE_OR_DIRECTORY);

        Operation operationToken;

        if (metaData.getType() == FILE)
            operationToken = writeFile(metaData);
        else
            operationToken = writeDirectory(metaData);

        return operationToken;
    }

    private Operation writeFile(EntityProperties metaData) throws ServiceException {

        final String path = normalizeFilePath(metaData.getPath());

        if (storageService.entityExists(path) || storageService
                .entityExists(normalizeDirectoryPath(path)))

            throw new ServiceException(ENTITY_ALREADY_EXISTS);

        File file = new File(path, metaData.getSize());

        StorageNodePair pair = duplicationService.findNodePairForFileSize(file.getSize());

        file = storageService.makeFile(file);

        duplicationService.addFile(file, pair);

        StorageNode master = duplicationService.getWriteNode(file);

        final String nodeAddress = formatNodeAddress(master.getAddress()
                , master.getPort());

        Operation operation = anOperation()
                .withOperation(SupportedOperations.WRITE)
                .withNodeAddress(nodeAddress)
                .withPath(path)
                .withSize(file.getSize())
                .build();

        operationRepository.save(operation);
        return operation;

    }

    private Operation writeDirectory(EntityProperties metaData) throws ServiceException {

        final String path = normalizeDirectoryPath(metaData.getPath());

        if (storageService.entityExists(path) || storageService
                .entityExists(normalizeFilePath(path)))

            throw new ServiceException(ENTITY_ALREADY_EXISTS);

        Directory directory = new Directory(path);
        storageService.makeDirectory(directory);

        return anOperation()
                .withOperation(SupportedOperations.WRITE)
                .withPath(path).build();

    }


    // List
    //=====================================================================================


    @Override
    public List<EntityProperties> listItemsWithNodes(String path) throws ServiceException {

        List<EntityProperties> properties = storageService.listItemsInDirectory(path);

        properties.forEach(entityProperties -> {
            if (entityProperties.getType() == FILE) {

                try {
                    entityProperties.setNodes(getNodeNames(entityProperties));
                } catch (ServiceException ignored) {
                }

            }
        });

        return properties;
    }


    @Override
    public EntityProperties getPropertiesWithNode(String path) throws ServiceException {

        EntityProperties property = storageService.getProperties(path);

        if (property.getType() == FILE)
            property.setNodes(getNodeNames(property));

        return property;

    }


    // Commit
    //=====================================================================================

    @Override
    @Transactional
    public synchronized void commitOperation(Operation operation) throws ServiceException {

        final File file = storageService.retrieveFile(operation.getPath());
        final StorageNode slave = duplicationService.getSlave(file);
        final StorageNode master = duplicationService.getWriteNode(file);

        if (!file.isAvailable())
            throw new ServiceException(FILE_IS_UNAVAILABLE);


        switch (operation.getOperation()) {

            case WRITE:
                if (slave != null) {

                    Operation pullOperation = anOperation()
                            .withOperation(SupportedOperations.PULL)
                            .withPath(file.getFilePath()).withSize(operation.getSize())
                            .withNodeAddress(operation.getNodeAddress())
                            .build();

                    nodeManagementService.sendAsyncOperation(pullOperation, slave);
                }

                nodeManagementService.updateNodeFreeSpace(master, -file.getSize());

                file.setWritten(true);
                storageService.makeFile(file);
                return;

            case DELETE:
                if (slave != null) {

                    Operation deleteOperation = anOperation()
                            .withOperation(SupportedOperations.DELETE_REPLICA)
                            .withPath(file.getFilePath()).withSize(operation.getSize())
                            .build();

                    nodeManagementService.sendAsyncOperation(deleteOperation, slave);

                } else {
                    duplicationService.deleteFile(file);
                    storageService.deleteFile(file);
                }

                nodeManagementService.updateNodeFreeSpace(master, file.getSize());
                return;

            case PULL:
                nodeManagementService.updateNodeFreeSpace(slave, -file.getSize());
                return;

            case DELETE_REPLICA:
                nodeManagementService.updateNodeFreeSpace(slave, file.getSize());
                duplicationService.deleteFile(file);
                storageService.deleteFile(file);
                return;

            default:
                throw new ServiceException(OPERATION_NOT_SUPPORTED);

        }

    }


    private List<String> getNodeNames(EntityProperties entityProperties) throws ServiceException {

        File file = storageService.retrieveFile(entityProperties.getPath());

        StorageNodePair pair = duplicationService.getStorageNodePair(file);

        List<String> nodeNames = new ArrayList<>();
        nodeNames.add(pair.getMaster().get_id());

        if (pair.getSlave() != null)
            nodeNames.add(pair.getSlave().get_id());
        return nodeNames;
    }
}
