package su.std1.gdfs.nameserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import su.std1.gdfs.nameserver.model.data.Operation;

@Repository
public interface OperationRepository extends MongoRepository<Operation, String> {
}
