package su.std1.gdfs.nameserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import su.std1.gdfs.nameserver.model.data.File;
import su.std1.gdfs.nameserver.model.data.StorageNodePair;

import java.util.Optional;

@Repository
public interface StorageNodePairRepository extends MongoRepository<StorageNodePair, String> {
    Optional<StorageNodePair> findByFilesContaining(File file);
}
