package su.std1.gdfs.nameserver.model.data;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Document
public class Directory {

    @Id
    private ObjectId _id;

    @Indexed(sparse = true)
    private String directoryPath;
    @DBRef(lazy = true)
    private List<File> files;

    @DBRef(lazy = true)
    private List<Directory> directories;

    @DBRef
    private Directory parent;


    public Directory() {
        super();
        files = new ArrayList<>();
        directories = new ArrayList<>();
    }

    public Directory(String directoryPath) {
        this();
        this.directoryPath = directoryPath.trim();
    }

    public Directory(String directoryPath, List<File> files, List<Directory> directories) {
        this(directoryPath);
        this.files = files;
        this.directories = directories;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath.trim();
    }

    public List<Directory> getDirectories() {
        return directories;
    }

    public void setDirectories(List<Directory> directories) {
        this.directories = directories;
    }

    public void addDirectory(Directory directory) {
        this.directories.add(directory);
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public void addFile(File file) {
        this.files.add(file);
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public Directory getParent() {
        return parent;
    }

    public void setParent(Directory parent) {
        this.parent = parent;
    }

    public void removeDirectory(Directory directory) {
        directories.remove(directory);
    }

    public void removeFile(File file) {
        files.remove(file);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Directory directory = (Directory) o;
        return Objects.equals(directoryPath, directory.directoryPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(directoryPath);
    }
}



