package su.std1.gdfs.nameserver.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.service.nodes.INodeManagementService;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.HEART_BEAT_INTERVAL;

@Component
public class ScheduledTaskRunner {


    private final INodeManagementService nodeManagementService;

    private Logger logger = LoggerFactory.getLogger(ScheduledTaskRunner.class);

    @Autowired
    public ScheduledTaskRunner(
            INodeManagementService nodeManagementService
    ) {
        this.nodeManagementService = nodeManagementService;
    }


    @Scheduled(fixedDelay = HEART_BEAT_INTERVAL)
    public void checkAllNodes() {
        try {
            nodeManagementService.pingNodes();
        } catch (ServiceException e) {
            logger.error("Could not switch nodes to dead");
        }
    }


}
