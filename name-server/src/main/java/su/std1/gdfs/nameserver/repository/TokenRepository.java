package su.std1.gdfs.nameserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import su.std1.gdfs.nameserver.security.model.Token;

@Repository
public interface TokenRepository extends MongoRepository<Token, String> {
    boolean existsByToken(String token);

}
