package su.std1.gdfs.nameserver.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.dto.ErrorResponse;

@ControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<?> serviceExceptions(ServiceException exception) {
        return ResponseEntity.badRequest().body(new ErrorResponse(exception.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> generalExceptions(Exception exception) {
        return ResponseEntity.badRequest().body(new ErrorResponse("Something Happened... :/"));

    }


    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<?> httpException(HttpClientErrorException exception) {

        String errorJson = exception.getResponseBodyAsString();
        ErrorResponse error;

        error = ErrorResponse.fromString(errorJson);

        if (error == null)
            error = new ErrorResponse(exception.getMessage());


        return ResponseEntity.badRequest().body(error);
    }

}
