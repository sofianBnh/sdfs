package su.std1.gdfs.nameserver.utils;


import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import su.std1.gdfs.nameserver.error.ServiceException;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.*;

public class CommonUtils {

    public static String formatNodeAddress(String ip, int port) {
        return String.format("%s:%d", ip, port);
    }

    public static String formatNodeUrl(String ip, int port) {
        return String.format("%s://%s", PROTOCOL, formatNodeAddress(ip, port));
    }

    public static boolean isLastCharacterSlash(String path) {
        return String.valueOf(path.charAt(path.length() - 1)).equals(PATH_DELIMITER);
    }


    public static String getParentDirPath(String fileOrDirPath) throws ServiceException {
        if (fileOrDirPath.length() <= 1)
            throw new ServiceException(IMPOSSIBLE_ACCESS_TO_PARENT_DIRECTORY);

        boolean endsWithSlash = fileOrDirPath.endsWith(PATH_DELIMITER);

        String parent = fileOrDirPath.substring(
                0,
                fileOrDirPath.lastIndexOf(
                        PATH_DELIMITER,
                        endsWithSlash ?
                                fileOrDirPath.length() - 2 :
                                fileOrDirPath.length() - 1
                )
        );

        return parent + PATH_DELIMITER;
    }

    public static void checkPathBasics(String path) throws ServiceException {

        if (path == null) throw new ServiceException(EMPTY_PATH);
        if (!String.valueOf(path.charAt(0)).equals(PATH_DELIMITER))
            throw new ServiceException(PATH_HAS_TO_ABSOLUTE);
    }

    public static String normalizeFilePath(String path) {

        if (isLastCharacterSlash(path))
            return path.substring(0, path.length() - 1);

        return path;
    }

    public static String normalizeDirectoryPath(String path) {

        if (!isLastCharacterSlash(path))
            return path + PATH_DELIMITER;

        return path;
    }


    public static RestTemplate getCustomRestTemplate() throws
            NoSuchAlgorithmException, KeyManagementException, KeyStoreException {

        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(
                sslContext, new NoopHostnameVerifier());

        CloseableHttpClient httpClient = HttpClients.custom()
                .setRetryHandler(new DefaultHttpRequestRetryHandler(
                        0,
                        false))
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setConnectionRequestTimeout(HEART_BEAT_INTERVAL);
        requestFactory.setConnectTimeout(HEART_BEAT_INTERVAL);
        requestFactory.setReadTimeout(HEART_BEAT_INTERVAL);

        requestFactory.setHttpClient(httpClient);

        return new RestTemplate(requestFactory);
    }
}
