package su.std1.gdfs.nameserver.service.nodes;

import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.Operation;
import su.std1.gdfs.nameserver.model.data.StorageNode;
import su.std1.gdfs.nameserver.model.data.StorageNodePair;
import su.std1.gdfs.nameserver.model.dto.NodeStatus;
import su.std1.gdfs.nameserver.model.dto.RegistrationAcknowledgment;

import java.util.List;

public interface INodeManagementService {

    RegistrationAcknowledgment registerNode(NodeStatus status, String ipAddress) throws ServiceException;

    void sendOperation(Operation operation, StorageNode node) throws ServiceException;

    void sendAsyncOperation(Operation operation, StorageNode node) throws ServiceException;

    void updateNodeFreeSpace(StorageNode nodes, long size) throws ServiceException;

    List<NodeStatus> getStatus() throws ServiceException;

    void pingNodes() throws ServiceException;

    void synchronize(StorageNodePair widowPair) throws ServiceException;

    void assignSlaves() throws ServiceException;
}
