package su.std1.gdfs.nameserver.config;

public class ApplicationConstants {

    public static final String NODE_CONTROL_ENDPOINT = "/storage/execute";
    public static final String NODE_PING_ENDPOINT = "/storage/ping";
    public static final String PATH_DELIMITER = "/";
    public static final long DIRECTORY_SIZE = 0;
    public static final String PROTOCOL = "https";

    public static final int HEART_BEAT_INTERVAL = 1000;

    // Errors

    public static final String EMPTY_PATH = "EMPTY_PATH";
    public static final String  BAD_CREDENTIALS = "BAD_CREDENTIALS";
    public static final String MASTER_NODE_NULL = "MASTER_NODE_NULL";
    public static final String FILE_IS_UNAVAILABLE = "FILE_IS_UNAVAILABLE";
    public static final String CANT_READ_DIRECTORY = "CANT_READ_DIRECTORY";
    public static final String INSUFFICIENT_STORAGE = "INSUFFICIENT_STORAGE";
    public static final String ROOT_CANT_BE_DELETED = "ROOT_CANT_BE_DELETED";
    public static final String PATH_HAS_TO_ABSOLUTE = "PATH_HAS_TO_ABSOLUTE";
    public static final String ENTITY_ALREADY_EXISTS = "ENTITY_ALREADY_EXISTS";
    public static final String DIRECTORY_IS_NOT_EMPTY = "DIRECTORY_IS_NOT_EMPTY";
    public static final String OPERATION_NOT_SUPPORTED = "OPERATION_NOT_SUPPORTED";
    public static final String COULD_NOT_FIND_NODE_PAIR = "COULD_NOT_FIND_NODE_PAIR";
    public static final String MORE_PAIRS_THAN_EXPECTED = "MORE_PAIRS_THAN_EXPECTED";
    public static final String NO_SUCH_FILE_OR_DIRECTORY = "NO_SUCH_FILE_OR_DIRECTORY";
    public static final String FILE_IN_USE = "FILE_IN_USE";
    public static final String UNEXPECTED_SINGLE_PAIR_COUNT = "UNEXPECTED_SINGLE_PAIR_COUNT";
    public static final String IMPOSSIBLE_ACCESS_TO_PARENT_DIRECTORY = "IMPOSSIBLE_ACCESS_TO_PARENT_DIRECTORY";
}
