package su.std1.gdfs.nameserver.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.Operation;
import su.std1.gdfs.nameserver.model.dto.NodeStatus;
import su.std1.gdfs.nameserver.service.nodes.INodeManagementService;
import su.std1.gdfs.nameserver.service.entity.IEntityManagementService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class NodesController {

    private final INodeManagementService nodeManagementService;
    private final IEntityManagementService resolutionService;

    @Autowired
    public NodesController(
            INodeManagementService nodeManagementService,
            IEntityManagementService resolutionService
    ) {
        this.nodeManagementService = nodeManagementService;
        this.resolutionService = resolutionService;
    }

    @PostMapping("nodes/register")
    public ResponseEntity<?> register(@RequestBody NodeStatus status, HttpServletRequest request)
            throws ServiceException {

        final String ipAddress = request.getRemoteAddr();
        return ResponseEntity.ok(nodeManagementService.registerNode(status, ipAddress));

    }

    @PostMapping("nodes/commit")
    public ResponseEntity<?> ackOperation(@RequestBody Operation operation) throws ServiceException {
        resolutionService.commitOperation(operation);
        return ResponseEntity.ok("");
    }


    @GetMapping("nodes/status")
    public ResponseEntity<?> getStatus() throws ServiceException {
        return ResponseEntity.ok(nodeManagementService.getStatus());
    }

}
