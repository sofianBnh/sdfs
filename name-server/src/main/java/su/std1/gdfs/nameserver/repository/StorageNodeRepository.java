package su.std1.gdfs.nameserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import su.std1.gdfs.nameserver.model.data.StorageNode;

import java.util.List;
import java.util.Optional;

@Repository
public interface StorageNodeRepository extends MongoRepository<StorageNode, String> {
    Optional<StorageNode> findByAddressAndPort(String address, int port);

    List<StorageNode> findAllByAlive(boolean alive);

}
