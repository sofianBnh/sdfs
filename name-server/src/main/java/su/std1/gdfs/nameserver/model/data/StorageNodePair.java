package su.std1.gdfs.nameserver.model.data;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Document
public class StorageNodePair {

    @Id
    private ObjectId _id;

    @DBRef
    private StorageNode master;

    @DBRef
    private StorageNode slave;

    @DBRef
    private List<File> files;

    public StorageNodePair() {
        files = new ArrayList<>();
    }

    public StorageNodePair(StorageNode master) {
        this();
        this.master = master;
    }

    public StorageNodePair(StorageNode master, StorageNode slave, List<File> files) {
        this(master);
        this.slave = slave;
        this.files = files;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public void addFile(File file) {
        this.files.add(file);
    }

    public void removeFile(File file) {
        this.files.remove(file);
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public StorageNode getMaster() {
        return master;
    }

    public void setMaster(StorageNode master) {
        this.master = master;
    }

    public StorageNode getSlave() {
        return slave;
    }

    public void setSlave(StorageNode slave) {
        this.slave = slave;
    }

    public void flipServers() {
        StorageNode temp = this.master;
        this.master = this.slave;
        this.slave = temp;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StorageNodePair nodePair = (StorageNodePair) o;
        return Objects.equals(_id.toHexString(), nodePair._id.toHexString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(_id.toHexString());
    }
}
