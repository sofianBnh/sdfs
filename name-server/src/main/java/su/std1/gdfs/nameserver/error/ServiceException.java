package su.std1.gdfs.nameserver.error;

public class ServiceException extends Exception {
    public ServiceException(String message) {
        super(message);
    }
}
