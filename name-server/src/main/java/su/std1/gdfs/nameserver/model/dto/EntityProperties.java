package su.std1.gdfs.nameserver.model.dto;

import su.std1.gdfs.nameserver.config.EntityType;

import java.util.List;

public class EntityProperties {


    private EntityType type;
    private String path;
    private long size;
    private List<String> node;


    public EntityProperties() {
    }

    public EntityProperties(EntityType type, long size, String path) {
        this.type = type;
        this.size = size;
        this.path = path.trim();
    }

    public EntityType getType() {
        return type;
    }

    public void setType(EntityType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path.trim();
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public List<String> getNode() {
        return node;
    }

    public void setNodes(List<String> node) {
        this.node = node;
    }
}
