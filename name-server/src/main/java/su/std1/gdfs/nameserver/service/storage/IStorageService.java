package su.std1.gdfs.nameserver.service.storage;

import su.std1.gdfs.nameserver.config.EntityType;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.Directory;
import su.std1.gdfs.nameserver.model.data.File;
import su.std1.gdfs.nameserver.model.dto.EntityProperties;

import java.util.List;

public interface IStorageService {

    List<EntityProperties> listItemsInDirectory(String path) throws ServiceException;

    EntityProperties getProperties(String path) throws ServiceException;

    boolean entityExists(String path) throws ServiceException;

    boolean parentDirectoryExists(String path) throws ServiceException;

    EntityType getType(String path) throws ServiceException;

    File makeFile(File file) throws ServiceException;

    Directory makeDirectory(Directory directory) throws ServiceException;

    File retrieveFile(String path) throws ServiceException;

    Directory retrieveDirectory(String path) throws ServiceException;

    void deleteFile(File file) throws ServiceException;

    void deleteDirectory(Directory directory) throws ServiceException;

    void changeAllFilesState(List<File> files, boolean avaialbe);
}
