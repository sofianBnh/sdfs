package su.std1.gdfs.nameserver.service.nodes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import su.std1.gdfs.nameserver.config.RegistrationFlags;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.data.Operation;
import su.std1.gdfs.nameserver.model.data.StorageNode;
import su.std1.gdfs.nameserver.model.data.StorageNodePair;
import su.std1.gdfs.nameserver.model.dto.NodeStatus;
import su.std1.gdfs.nameserver.model.dto.RegistrationAcknowledgment;
import su.std1.gdfs.nameserver.repository.StorageNodeRepository;
import su.std1.gdfs.nameserver.service.duplica.IDuplicationService;
import su.std1.gdfs.nameserver.service.recovery.IRecoveryService;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.*;
import static su.std1.gdfs.nameserver.config.SupportedOperations.PULL;
import static su.std1.gdfs.nameserver.model.data.Operation.OperationBuilder.anOperation;
import static su.std1.gdfs.nameserver.utils.CommonUtils.*;

@Service
public class NodeManagementService implements INodeManagementService {


    private final RestTemplate restClient;
    private final IRecoveryService recoveryService;
    private final IDuplicationService duplicationService;
    private final StorageNodeRepository storageNodeRepository;
    private final Logger logger = LoggerFactory.getLogger(NodeManagementService.class);

    @Autowired
    public NodeManagementService(
            IRecoveryService recoveryService,
            IDuplicationService duplicationService,
            StorageNodeRepository storageNodeRepository
    ) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        this.recoveryService = recoveryService;
        this.duplicationService = duplicationService;
        this.restClient = getCustomRestTemplate();
        this.storageNodeRepository = storageNodeRepository;
    }

    @Override
    public void sendOperation(Operation operation, StorageNode node) {

        operation.sign();

        String url = formatNodeUrl(node.getAddress(), node.getPort());
        HttpEntity<Operation> operationEntity = new HttpEntity<>(operation);

        restClient.postForEntity(url + NODE_CONTROL_ENDPOINT,
                operationEntity, String.class
        );

    }

    @Async
    @Override
    public void sendAsyncOperation(Operation operation, StorageNode node) {
        sendOperation(operation, node);
    }

    @Override
    public void updateNodeFreeSpace(StorageNode node, long size) {
        node.setFree(node.getFree() + size);
        storageNodeRepository.save(node);
    }

    @Override
    public List<NodeStatus> getStatus() throws ServiceException {

        return storageNodeRepository.findAll().stream()
                .filter(StorageNode::isAlive)
                .map(
                        storageNode -> new NodeStatus(
                                storageNode.getCapacity(),
                                storageNode.getFree(),
                                storageNode.get_id(),
                                storageNode.getPort()
                        )
                ).collect(Collectors.toList());
    }

    @Override
    public void pingNodes() throws ServiceException {

        storageNodeRepository.findAll()
                .parallelStream()
                .forEach(node -> {
                    String url = formatNodeUrl(node.getAddress(), node.getPort());
                    try {
                        restClient.getForEntity(url + NODE_PING_ENDPOINT, String.class);
                        node.setAlive(true);
                    } catch (HttpClientErrorException | ResourceAccessException e) {
                        node.setAlive(false);
                    }
                    storageNodeRepository.save(node);
                });

        recoveryService.handleWidowPairs();

        List<StorageNodePair> resurrectedPairs = recoveryService.findResurrectedPairs();
        recoveryService.handleResurrectedPairs(resurrectedPairs);

        assignSlaves();

    }


    @Override
    @Transactional
    public synchronized RegistrationAcknowledgment registerNode(NodeStatus status, String ipAddress)
            throws ServiceException {

        Optional<StorageNode> sameAddressAndPort = storageNodeRepository
                .findByAddressAndPort(ipAddress, status.getPort());

        RegistrationAcknowledgment registrationAck = new RegistrationAcknowledgment();

        if (sameAddressAndPort.isPresent()) {

            StorageNode node = sameAddressAndPort.get();

            List<StorageNodePair> pairs = duplicationService.getPairByNode(node);

            if (pairs.size() == 0) {
                node.setFree(node.getCapacity());
                storageNodeRepository.save(node);
            }

            AtomicBoolean masterForPair = new AtomicBoolean(false);

            pairs.forEach(pair -> {
                if (pair.getMaster().equals(node))
                    masterForPair.set(true);
            });


            if (masterForPair.get())
                registrationAck.setFlag(RegistrationFlags.KEEP_DATA);

            else {

                node.setFree(node.getCapacity());
                storageNodeRepository.save(node);

                duplicationService.buildPairOrSlaveAvailable(node);

                registrationAck.setFlag(RegistrationFlags.FORMAT_DISK);

            }

            registrationAck.setNodeName(node.get_id());

        } else {

            StorageNode node = new StorageNode(
                    ipAddress,
                    status.getPort(),
                    status.getCapacity(),
                    status.getCapacity()
            );

            StorageNode saved = storageNodeRepository.save(node);
            duplicationService.buildPairOrSlaveAvailable(saved);

            registrationAck.setFlag(RegistrationFlags.FORMAT_DISK);
            registrationAck.setNodeName(saved.get_id());
        }

        return registrationAck;
    }


    @Override
    @Transactional
    public synchronized void assignSlaves() throws ServiceException {

        List<StorageNodePair> singlePairs = duplicationService.getSinglePairs();
        singlePairs = singlePairs
                .stream()
                .filter(pair -> pair.getMaster().isAlive())
                .sorted(Comparator.comparing(pair -> pair.getMaster().getFree()))
                .collect(Collectors.toList());


        List<StorageNode> freeNodes = duplicationService.getFreeNodes();

        singlePairs.forEach(pair -> {

            if (pair.getMaster() == null)
                throw new NullPointerException(MASTER_NODE_NULL);

            if (!freeNodes.isEmpty()) {
                duplicationService.setSlave(freeNodes.get(0), pair);
                freeNodes.remove(0);
                try {
                    synchronize(pair);
                } catch (ServiceException ignored) {
                }
            }

        });

    }

    @Override
    @Transactional
    public synchronized void synchronize(StorageNodePair updatedPair) throws ServiceException {


        StorageNode master = updatedPair.getMaster();
        StorageNode slave = updatedPair.getSlave();

        if (master == null)
            return;

        final String masterAddress = formatNodeAddress(master.getAddress()
                , updatedPair.getMaster().getPort());


        if (updatedPair.getSlave() != null && updatedPair.getSlave().isAlive()) {

            updatedPair.getFiles().forEach(file -> {
                Operation pullFromMaster = anOperation().withNodeAddress(masterAddress)
                        .withOperation(PULL)
                        .withPath(file.getFilePath())
                        .withSize(file.getSize())
                        .build();

                sendOperation(pullFromMaster, slave);

            });
        }

    }


}



