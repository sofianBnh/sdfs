package su.std1.gdfs.nameserver.security;


import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(POST, "/storage").permitAll()
                .antMatchers(POST, "/auth").permitAll()
                .antMatchers(DELETE, "/storage").permitAll()
                .antMatchers(GET, "/storage").permitAll()
                .antMatchers(GET, "/properties").permitAll()
                .antMatchers(GET, "/list").permitAll()
                .antMatchers(POST, "/nodes/**").permitAll()
                .antMatchers(GET, "/nodes/**").permitAll()
                .anyRequest().authenticated();
    }

}
