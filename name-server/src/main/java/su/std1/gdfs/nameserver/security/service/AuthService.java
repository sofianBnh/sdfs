package su.std1.gdfs.nameserver.security.service;

import org.springframework.stereotype.Service;
import su.std1.gdfs.nameserver.error.ServiceException;
import su.std1.gdfs.nameserver.model.dto.Login;
import su.std1.gdfs.nameserver.repository.TokenRepository;
import su.std1.gdfs.nameserver.repository.UserRepository;
import su.std1.gdfs.nameserver.security.model.Token;
import su.std1.gdfs.nameserver.security.model.User;

import java.util.Optional;
import java.util.UUID;

import static su.std1.gdfs.nameserver.config.ApplicationConstants.BAD_CREDENTIALS;

@Service
public class AuthService {

    private final TokenRepository tokenRepository;
    private final UserRepository userRepository;

    public AuthService(TokenRepository tokenRepository, UserRepository userRepository) {
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
    }


    public void verifyToken(String token) throws ServiceException {

        if (!tokenRepository.existsByToken(token))
            throw new ServiceException(BAD_CREDENTIALS);


    }

    public Token generateToken(Login login) throws ServiceException {

        if (login.getUsername() == null || login.getPassword() == null)
            throw new ServiceException(BAD_CREDENTIALS);

        Optional<User> user = userRepository.findByUsername(login.getUsername());

        if (!user.isPresent())
            throw new ServiceException(BAD_CREDENTIALS);

        if (!user.get().getPassword().equals(login.getPassword()))
            throw new ServiceException(BAD_CREDENTIALS);

        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();

        Token token = new Token(randomUUIDString);
        tokenRepository.save(token);

        return token;
    }

}
