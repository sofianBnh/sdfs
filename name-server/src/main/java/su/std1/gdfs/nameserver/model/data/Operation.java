package su.std1.gdfs.nameserver.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import su.std1.gdfs.nameserver.config.SupportedOperations;

import java.rmi.ServerException;
import java.sql.Timestamp;
import java.util.Objects;


@Document
public class Operation {

    @Indexed
    private String digest;

    @Id
    @JsonIgnore
    private ObjectId _id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Timestamp timestamp;
    private SupportedOperations operation;
    private String nodeAddress;
    private String path;
    private long size;

    public Operation() {
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    private Operation(
            SupportedOperations operation,
            String nodeAddress,
            String path,
            long size
    ) {
        this();
        this.operation = operation;
        this.nodeAddress = nodeAddress;
        this.path = path;
        this.size = size;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public SupportedOperations getOperation() {
        return operation;
    }

    public void setOperation(SupportedOperations operation) {
        this.operation = operation;
    }

    public String getNodeAddress() {
        return nodeAddress;
    }

    public void setNodeAddress(String nodeAddress) {
        this.nodeAddress = nodeAddress;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void sign() {

    }

    public void verify() throws ServerException {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return Objects.equals(_id.toHexString(), operation._id.toHexString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(_id.toHexString());
    }

    public static final class OperationBuilder {
        private SupportedOperations operation;
        private String nodeAddress;
        private String path;
        private long size;

        private OperationBuilder() {
        }

        public static OperationBuilder anOperation() {
            return new OperationBuilder();
        }

        public OperationBuilder withOperation(SupportedOperations operation) {
            this.operation = operation;
            return this;
        }

        public OperationBuilder withNodeAddress(String nodeAddress) {
            this.nodeAddress = nodeAddress;
            return this;
        }

        public OperationBuilder withPath(String path) {
            this.path = path;
            return this;
        }

        public OperationBuilder withSize(long size) {
            this.size = size;
            return this;
        }


        public Operation build() {
            return new Operation(operation, nodeAddress, path, size);
        }
    }
}



